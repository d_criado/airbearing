/****************************************************************************
 * 
 * Platform test debugging
 *
 * LED BLINK ERROR CODES:
 * Serial port failed                 Continuous blink (5 per second) 
 * Radio begin failed                 1 per second
 * IMU begin failed                   3 per second
 * DATA operational mode without IMU  2 per second
 ***************************************************************************/

#include <platform.h>
Platform platform(true, true, true);

void setup()
{
    platform.begin();
}

void loop() 
{
    platform.listenCommand();
    platform.operateCommand();
    // Serial.println("It works!");
    // delay(20);
}

/* System Status (see section 4.3.58)
     ---------------------------------
     0 = Idle
     1 = System Error
     2 = Initializing Peripherals
     3 = System Iniitalization
     4 = Executing Self-Test
     5 = Sensor fusio algorithm running
     6 = System running without fusion algorithms */

/* Self Test Results (see section )
     --------------------------------
     1 = test passed, 0 = test failed
     
     Bit 0 = Accelerometer self test
     Bit 1 = Magnetometer self test
     Bit 2 = Gyroscope self test
     Bit 3 = MCU self test
     
     0x0F = all good! */

/* System Error (see section 4.3.59)
     ---------------------------------
     0 = No error
     1 = Peripheral initialization error
     2 = System initialization error
     3 = Self test result failed
     4 = Register map value out of range
     5 = Register map address out of range
     6 = Register map write error
     7 = BNO low power mode not available for selected operat ion mode
     8 = Accelerometer power mode not available
     9 = Fusion algorithm configuration error
     A = Sensor configuration error */
