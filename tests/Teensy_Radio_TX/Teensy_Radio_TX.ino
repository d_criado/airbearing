/*
 * TEENSY 3.5 PINOUT SETUP
 * -----------------------------
 * RST        OUTPUT    34
 * G0 (IRQ)   INPUT     33
 * CS         OUTPUT    15 (CS0)
 * MOSI       -         28 (MOSI0)
 * MISO       -         39 (MISO0)
 * SCK        -         14 (SCK0)
 * EN (NO CONNECTED)
 * GND        -         GND
 * VIN        -         3.3V
 */

#include <SPI.h>
#include <RH_RF69.h>

#define RF69_FREQ     433.0
#define LED           13
#define RFM69_RST     34
#define RFM69_INT     33
#define RFM69_CS      15
#define LED           13

/// Constructor. You can have multiple instances, but each instance must have its own
/// interrupt and slave select pin. After constructing, you must call init() to initialise the interface
/// and the radio module. A maximum of 3 instances can co-exist on one processor, provided there are sufficient
/// distinct interrupt lines, one for each instance.
/// \param[in] slaveSelectPin the Arduino pin number of the output to use to select the RF69 before
/// accessing it. Defaults to the normal SS pin for your Arduino (D10 for Diecimila, Uno etc, D53 for Mega, D10 for Maple)
/// \param[in] interruptPin The interrupt Pin number that is connected to the RF69 DIO0 interrupt line. 
/// Defaults to pin 2.
/// Caution: You must specify an interrupt capable pin.
/// On many Arduino boards, there are limitations as to which pins may be used as interrupts.
/// On Leonardo pins 0, 1, 2 or 3. On Mega2560 pins 2, 3, 18, 19, 20, 21. On Due and Teensy, any digital pin.
/// On Arduino Zero from arduino.cc, any digital pin other than 4.
/// On Arduino M0 Pro from arduino.org, any digital pin other than 2.
/// On other Arduinos pins 2 or 3. 
/// See http://arduino.cc/en/Reference/attachInterrupt for more details.
/// On Chipkit Uno32, pins 38, 2, 7, 8, 35.
/// On other boards, any digital pin may be used.
/// \param[in] spi Pointer to the SPI interface object to use. 
///                Defaults to the standard Arduino hardware SPI interface
RH_RF69 rf69(RFM69_CS, RFM69_INT);

int16_t packetnum = 0;  // packet counter, we increment per xmission

void setup()
{
  Serial.begin(115200);
  while(!Serial){
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }


  SPI.begin();
  SPI.setSCK(14);
  SPI.setMOSI(28);
  SPI.setMISO(39);
  

  pinMode(LED, OUTPUT);
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  Serial.println("Feather RFM69 TX Test!");
  Serial.println();

  // manual reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);


  /// Initialises this instance and the radio module connected to it.
  /// The following steps are taken:
  /// - Initialise the slave select pin and the SPI interface library
  /// - Checks the connected RF69 module can be communicated
  /// - Attaches an interrupt handler
  /// - Configures the RF69 module
  /// - Sets the frequency to 434.0 MHz
  /// - Sets the modem data rate to FSK_Rb2Fd5
  /// \return  true if everything was successful
  if (!rf69.init()) {
    Serial.println("RFM69 radio init failed");
    while (1);
  }
  Serial.println("RFM69 radio init OK!");
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  // No encryption
  if (!rf69.setFrequency(RF69_FREQ)) {
    Serial.println("setFrequency failed");
  }

  // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
  // ishighpowermodule flag set like this:
  rf69.setTxPower(20, true);  // range from 14-20 for power, 2nd arg must be true for 69HCW

  // The encryption key has to be the same as the one in the server
  uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08
                  };
  rf69.setEncryptionKey(key);

  pinMode(LED, OUTPUT);

  Serial.print("RFM69 radio @");  Serial.print((int)RF69_FREQ);  Serial.println(" MHz");
}



void loop() {
  delay(1000);  // Wait 1 second between transmits, could also 'sleep' here!

  char radiopacket[20] = "Hello World #";
  itoa(packetnum++, radiopacket + 13, 10);
  Serial.print("Sending "); Serial.println(radiopacket);

  // Send a message!
  rf69.send((uint8_t *)radiopacket, strlen(radiopacket));
  rf69.waitPacketSent();

  // Now wait for a reply
  uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(buf);

  if (rf69.waitAvailableTimeout(500))  {
    // Should be a reply message for us now
    if (rf69.recv(buf, &len)) {
      Serial.print("Got a reply: ");
      Serial.println((char*)buf);
      Blink(LED, 50, 3); //blink LED 3 times, 50ms between blinks
    } else {
      Serial.println("Receive failed");
    }
  } else {
    Serial.println("No reply, is another RFM69 listening?");
  }
}

void Blink(byte PIN, byte DELAY_MS, byte loops) {
  for (byte i = 0; i < loops; i++)  {
    digitalWrite(PIN, HIGH);
    delay(DELAY_MS);
    digitalWrite(PIN, LOW);
    delay(DELAY_MS);
  }
}
