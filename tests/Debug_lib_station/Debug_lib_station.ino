/****************************************************************************
 * Air Bearing - Station function suite
 * David Criado Pernia
 * 2019
 * 
 * EXAMPLE
 * Code to be compiled in Station
 *
 * LED BLINK ERROR CODES
 * ==========================================================================
 * 
 *
 ***************************************************************************/

// Include Station library
#include <station.h>

// Declare instance of class Station
// No args
Station station = Station();

// Prepare environment
void setup()
{
    // Init devices, check IMU calibration
    station.begin();
}

// Run infinitely
void loop()
{
    // Listen if any telecommand (TC) has been sent from serial USB or Radio
    // A: Operating mode, B & C: Parameters
    station.listen();
    // Operate according to selected Operating Mode and parameters
    station.operate();
}
