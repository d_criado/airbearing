# List ports: python -m serial.tools.list_ports

import serial
import time
import sys

ser = serial.Serial('COM4', 115200, timeout=0)

while(1):
    line = ser.readline().decode().strip('\r\n')
    if len(line) > 0:
        print(line)
    # time.sleep(0.05)
