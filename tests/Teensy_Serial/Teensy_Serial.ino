#define LED           13

void setup() {
  pinMode(LED, OUTPUT);
  Serial.begin(115200);
  while(!Serial){
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }
}

void loop() {
  Serial.println("Teensy Serial works");
  digitalWrite(LED, HIGH);
  delay(1000);
  digitalWrite(LED, LOW);
  delay(1000);
}
