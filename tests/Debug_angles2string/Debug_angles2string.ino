/*
 * TEENSY 3.5 PINOUT SETUP
 * -------------------------- RF ---
 * RST        OUTPUT    34
 * G0 (IRQ)   INPUT     33
 * CS         OUTPUT    15 (CS0)
 * MOSI       -         28 (MOSI0)
 * MISO       -         39 (MISO0)
 * SCK        -         14 (SCK0)
 * EN (NO CONNECTED)
 * GND        -         GND
 * VIN        -         3.3V
 * ------------------------- IMU ---
 * RST        OUTPUT    23
 * SCK        OUTPUT    18
 * SDA        OUTPUT    19
 */

/************ Libraries *****************/

#include <SPI.h>
#include <RH_RF69.h>
#include <RHReliableDatagram.h>

#include <i2c_t3.h>                 // More info: https://goo.gl/RyzFaM
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055_t3.h>     // More info: https://goo.gl/eAYKjn
#include <utility/imumaths.h>

/************ Radio Setup ***************/

// Freq
#define RF69_FREQ     433.0
// Addresses
#define DEST_ADDRESS   1            // Where to send packets to (server address)
#define MY_ADDRESS     2            // Change addresses for each client board, any number :)
// Pins
#define LED           13
#define RFM69_RST     34
#define RFM69_INT     33
#define RFM69_CS      15
#define RFM69_SCK     14
#define RFM69_MOSI    28
#define RFM69_MISO    39

/************ IMU Setup ****************/

// Delay between samples
#define BNO055_SAMPLERATE_DELAY_MS (100)
// Pins
#define BNO_RST     23
#define LED         13

/********* Vars declaration ************/

// Radio
RH_RF69 rf69(RFM69_CS, RFM69_INT);                 // Singleton instance of the radio driver
RHReliableDatagram rf69_manager(rf69, MY_ADDRESS); // Class to manage message delivery and receipt, using the driver declared above
int16_t packetnum = 0;                             // packet counter

// IMU
Adafruit_BNO055 bno = Adafruit_BNO055(WIRE_BUS, -1, BNO055_ADDRESS_A, I2C_MASTER, 
                                      I2C_PINS_18_19, I2C_PULLUP_EXT, I2C_RATE_100, I2C_OP_MODE_ISR);


void setup() 
{
  // Init system
  // ====================================================================
  
  // Init serial
  Serial.begin(115200);
  while(!Serial){
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }

  // Init SPI
  SPI.begin();
  SPI.setSCK(RFM69_SCK);
  SPI.setMOSI(RFM69_MOSI);
  SPI.setMISO(RFM69_MISO);

  // Init LED and RST pins
  pinMode(LED, OUTPUT);     
  pinMode(RFM69_RST, OUTPUT);
  pinMode(BNO_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);
  pinMode(BNO_RST, LOW);

  // Prompt init information
  Serial.println("Teensy + IMU + Radio Test");
  Serial.println("  Inited Serial, SPI, pins");

  // Init radio
  // ====================================================================
  
  // Manual radio reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);
  
  if (!rf69_manager.init()) {
    Serial.println("RFM69 radio init failed");
    while(1);
  }
  Serial.println("  Inited RFM69 radio. Setting up radio");
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  // No encryption
  if (!rf69.setFrequency(RF69_FREQ)) {
    Serial.println("setFrequency failed");
  }

  // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
  // ishighpowermodule flag set like this:
  rf69.setTxPower(20, true);  // range from 14-20 for power, 2nd arg must be true for 69HCW

  // The encryption key has to be the same as the one in the server
  uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  rf69.setEncryptionKey(key);
  Serial.print("  RFM69 radio @");  Serial.print((int)RF69_FREQ);  Serial.println(" MHz");

  // Init IMU
  // ====================================================================

  // Manual IMU reset
  digitalWrite(BNO_RST, HIGH);
  delay(10);
  digitalWrite(BNO_RST, LOW);
  delay(10);

  if(!bno.begin()) {
    Serial.print("No BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }
  Serial.println("  Inited BNO055 IMU. Setting up IMU");

  delay(500);

  /* Display the current temperature */
  int8_t temp = bno.getTemp();
  Serial.print("    Current Temperature: ");
  Serial.print(temp);
  Serial.println(" C");

  setCal();
  bno.setExtCrystalUse(true);
  bno.setMode(bno.OPERATION_MODE_NDOF);   // Set Operational mode

  Serial.println("IMU calibrated and ready");
}


// Dont put this on the stack:
uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
uint8_t data[] = "  OK";

void loop() {
  delay(10);  // Wait 10 ms
  
  imu::Quaternion quat = bno.getQuat();
  imu::Vector<3>  euler = quat.toEuler();

  Serial.print(euler[0]);Serial.print("\t");
  Serial.print(euler[1]);Serial.print("\t");
  Serial.println(euler[2]);

  // Loop radio
  // ====================================================================
  char radiopacket[64];
  String eu0, eu1, eu2;
  eu0 = String(euler[0]);
  eu1 = String(euler[1]);
  eu2 = String(euler[2]);

  // Assemble data
  sprintf(radiopacket, "%s,%s,%s", eu0.c_str(), eu1.c_str(), eu2.c_str());
  
  Serial.print("Sending "); Serial.println(radiopacket);
  
  // Send a message to the DESTINATION!
  if (rf69_manager.sendtoWait((uint8_t *)radiopacket, strlen(radiopacket), DEST_ADDRESS)) {
    // Now wait for a reply from the server
    uint8_t len = sizeof(buf);
    uint8_t from;
    if (rf69_manager.recvfromAckTimeout(buf, &len, 10, &from)) {
      buf[len] = 0; // zero out remaining string
      
      Serial.print("Got reply from #"); Serial.print(from);
      Serial.print(" [RSSI :");
      Serial.print(rf69.lastRssi());
      Serial.print("] : ");
      Serial.println((char*)buf);     
      Blink(LED, 10, 3); //blink LED 3 times, 10ms between blinks
    } else {
      Serial.println("No reply, is anyone listening?");
    }
  } else {
    Serial.println("Sending failed (no ack)");
  }
}

// FUNCTIONS
// ====================================================================

void Blink(byte PIN, byte DELAY_MS, byte loops) {
  for (byte i=0; i<loops; i++)  {
    digitalWrite(PIN,HIGH);
    delay(DELAY_MS);
    digitalWrite(PIN,LOW);
    delay(DELAY_MS);
  }
}

void setCal(){
    const uint8_t calibrationData[22] = {uint8_t lowByte(65522),uint8_t highByte(65522),
                                     uint8_t lowByte(65499),uint8_t highByte(65499),
                                     uint8_t lowByte(27),   uint8_t highByte(27),
                                                 
                                     uint8_t lowByte(65535),uint8_t highByte(65535),
                                     uint8_t lowByte(2),    uint8_t highByte(2),
                                     uint8_t lowByte(1),    uint8_t highByte(1),
                                                 
                                     uint8_t lowByte(172),  uint8_t highByte(172),
                                     uint8_t lowByte(65316),uint8_t highByte(65316),
                                     uint8_t lowByte(155),  uint8_t highByte(155),
                                                 
                                     uint8_t lowByte(1000), uint8_t highByte(1000),
                                     uint8_t lowByte(475),  uint8_t highByte(475)};
    
    bno.setSensorOffsets(calibrationData);
    delay(4000);
    Serial.println("IMU: offsets added");
    // Check if IMU is calibrated
    if(bno.isFullyCalibrated()){
        Serial.println("IMU: fully calibrated");
    } 
}
