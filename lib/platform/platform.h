/**************************************************************************
 *  Air Bearing - Platform function suite
 *  David Criado Pernia
 *  2019
 *
 *  HARDWARE
 *  ------------------------------------------
 *    Teensy 3.5            Controller
 *    Adafruit BNO055       IMU
 *    Adafruit RFM69HCW     Radio TXR
 *
 *  CONTENTS
 *  ------------------------------------------
 *  1. 
 *
 **************************************************************************/

// Prevent double declaration
#ifndef __PLATFORM_H__
#define __PLATFORM_H__

// Libraries
#include <SPI.h>
#include <RH_RF69.h>
#include <RHReliableDatagram.h>

#include <i2c_t3.h>             // More info: https://goo.gl/RyzFaM
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055_t3.h> // More info: https://goo.gl/eAYKjn
#include <utility/imumaths.h>

/**************************************************************************
 *  RADIO vars
 **************************************************************************/

// Freq
#define RF69_FREQ       (433.0)

// Addresses
#define DEST_ADDRESS    (1)     // Where to send packets to (server address)
#define MY_ADDRESS      (2)     // Change addresses for each client board, any number :)

// Pins
#define LED             (13)
#define RFM69_RST       (34)
#define RFM69_INT       (33)
#define RFM69_CS        (15)
#define RFM69_SCK       (14)
#define RFM69_MOSI      (28)
#define RFM69_MISO      (39)

/**************************************************************************
 *  IMU vars
 **************************************************************************/

// Pins
#define BNO_RST         (23)

/**************************************************************************
 *  CLASS
 **************************************************************************/
class Platform {
    public:
        bool HAS_SERIAL;

        Platform();

        // Begin platform
        bool begin          (void);
        bool beginIMU       (void);
        bool beginRadio     (void);
        bool beginSerial    (void);
        bool beginTeensy    (void);

    private:

        // Declare instances of sensor classes
        Adafruit_BNO055 bno(WIRE_BUS, -1, BNO055_ADDRESS_A, I2C_MASTER,
                            I2C_PINS_18_19, I2C_PULLUP_EXT, I2C_RATE_100, I2C_OP_MODE_ISR);
        RH_RF69 rf69(RFM69_CS, RFM69_INT);                 // Singleton instance of the radio driver
        RHReliableDatagram rf69_manager(rf69, MY_ADDRESS); // Class to manage message delivery and receipt, using the driver declared above

        bool prompt(char *message);
        bool alert(char* message);
        bool trigger(int pin, int delta);
        void calibOffsetsIMU();
};
#endif
