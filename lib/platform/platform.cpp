/**************************************************************************
 *  Air Bearing - Platform function suite
 *  David Criado Pernia
 *  2019
 *
 *  HARDWARE
 *  ------------------------------------------
 *    Teensy 3.5            Controller
 *    Adafruit BNO055       IMU
 *    Adafruit RFM69HCW     Radio txr
 *
 *  CONTENTS
 *  ------------------------------------------
 *  1. 
 *
 **************************************************************************/

/*
 * TEENSY 3.5 PINOUT SETUP
 * -------------------------- RF ---
 * RST        OUTPUT    34
 * G0 (IRQ)   INPUT     33
 * CS         OUTPUT    15 (CS0)
 * MOSI       -         28 (MOSI0)
 * MISO       -         39 (MISO0)
 * SCK        -         14 (SCK0)
 * EN (NO CONNECTED)
 * GND        -         GND
 * VIN        -         3.3V
 * ------------------------- IMU ---
 * RST        OUTPUT    23
 * SCK        OUTPUT    18
 * SDA        OUTPUT    19
 */

#include "platform.h"

// Constructor
Platform::Platform(){
    // insert here initialization of internal vars and stuff
}

/**************************************************************************
 *  BEGIN functions
 **************************************************************************/

// Begin Teensy, Radio, IMU and Serial (only if applies)
bool Platform::begin(bool HAS_SERIAL = false)
{
    // Check if Receiver is set to have a Serial connection
    if (HAS_SERIAL)
    {
        // Start USB Serial connection
        if (!beginSerial()) alert();
    }

    // Start Teensy
    if (!beginTeensy()) alert("ERROR: Teensy cannot be booted. Initialization stopped.");

    // Start Radio
    if (!beginRadio()) alert("ERROR: Radio cannot be booted. Initialization stopped.");

    // Start IMU
    if (!beginIMU()) alert("ERROR: IMU cannot be booted. Initialization stopped.");

    // Test Radio link health before operation modes and prompt results
    // testHandshake();
}

// Begin IMU
bool Platform::beginIMU()
{
    // Manual reset
    trigger(BNO_RST);

    // Start sensor
    if (!bno.begin()) return false;
    prompt("Inited BNO055 IMU. Setting up IMU...");
    delay(1000);

    // Set offsets, clock and operation mode
    calibOffsetsIMU();
    bno.setExtCrystalUse(true);
    bno.setMode(bno.OPERATION_MODE_NDOF); // Set Operational mode
    prompt("IMU calibrated and ready");
    return true;
}

// Begin radio transceiver
bool Platform::beginRadio()
{
    // Manual reset
    trigger(RFM69_RST);

    // Start sensor
    if (!rf69_manager.init()) return false;
    prompt("Inited RFM69 radio. Setting up radio...");

    
    if (!rf69.setFrequency(RF69_FREQ)) return false; // Defaults 434MHz, GFSK_Rb250Fd250, +13dbM
    rf69.setTxPower(20, true); // Range from 14-20 for power, 2nd arg must be true for 69HCW
    uint8_t key[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                     0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}; // Key same as server
    rf69.setEncryptionKey(key);
    prompt("Radio configured and ready");
}

// Begin Serial port
bool Platform::beginSerial()
{
    Serial.begin(BAUDRATE ? BAUDRATE : 115200);
    if (Serial) return true;
    return false;
}

// Begin Teensy: init pins and SPI
bool Platform::beginTeensy()
{
    // Init LED and RST pins
    pinMode(LED, OUTPUT);
    pinMode(RFM69_RST, OUTPUT);
    pinMode(BNO_RST, OUTPUT);

    // Set RST pins low
    digitalWrite(RFM69_RST, LOW);
    digitalWrite(BNO_RST, LOW);

    // Init SPI
    if (!SPI.begin())
    {
        return false;
    }
    SPI.setSCK(RFM69_SCK);
    SPI.setMOSI(RFM69_MOSI);
    SPI.setMISO(RFM69_MISO);

    return true;
}

/**************************************************************************
 *  AUX functions
 **************************************************************************/

// Send message by Radio and Serial if connected
void Platform::prompt(char* message = false)
{
    if (message)
    {
        // TODO: Send by RF if radio enabled
        if (HAS_SERIAL)
        {
            Serial.println(message);
        }
    }
}
// Alert of error by blinking LED and prompting message
void Platform::alert(char* message = false)
{
    if (HAS_SERIAL && message)
    {
        Serial.println(message);
    }
    while (1)
    {
        trigger(LED, 100);
    }
}

// Blink LED or reset device by triggering RST PIN
void Platform::trigger(int PIN == LED, int delta == 10)
{
    digitalWrite(PIN, HIGH);
    delay(delta);
    digitalWrite(PIN, LOW);
    delay(delta);
}

/**************************************************************************
 *  IMU functions
 **************************************************************************/

// Calibrate IMU offsets
void Platform::calibOffsetsIMU()
{
    const uint8_t calibrationData[22] = {uint8_t lowByte(65522), uint8_t highByte(65522),
                                         uint8_t lowByte(65499), uint8_t highByte(65499),
                                         uint8_t lowByte(27), uint8_t highByte(27),

                                         uint8_t lowByte(65535), uint8_t highByte(65535),
                                         uint8_t lowByte(2), uint8_t highByte(2),
                                         uint8_t lowByte(1), uint8_t highByte(1),

                                         uint8_t lowByte(172), uint8_t highByte(172),
                                         uint8_t lowByte(65316), uint8_t highByte(65316),
                                         uint8_t lowByte(155), uint8_t highByte(155),

                                         uint8_t lowByte(1000), uint8_t highByte(1000),
                                         uint8_t lowByte(475), uint8_t highByte(475)};

    bno.setSensorOffsets(calibrationData);
    delay(4000);
    Serial.println("IMU: offsets added");
    // Check if IMU is calibrated
    if (bno.isFullyCalibrated())
    {
        Serial.println("IMU: fully calibrated");
    }
}